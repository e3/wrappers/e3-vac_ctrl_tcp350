require vac_ctrl_tcp350

epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tcp350_DB}")
iocshLoad(${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh, "DEVICENAME = vept, IPADDR = localhost, PORT = 4000")
iocshLoad(${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh, "DEVICENAME = vpt, CONTROLLERNAME = vept")
